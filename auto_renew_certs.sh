#!/bin/bash

# Make sure only root can run our script
if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

domains=(\
"abossy.fr" \
"rpi2.abossy.fr" \
"rss.abossy.fr" \
"cozy.abossy.fr"\
);

path_to_certbot='/home/admin/certbot-auto'

for domain in ${domains[*]}
do
	echo "Checking if cert needs to be updated for " $domain
	openssl x509 -checkend 2592000 -noout -in /etc/letsencrypt/live/$domain/cert.pem
	if [ $? -gt 0 ]; then
		echo "Cert needs update"
		systemctl status nginx >> /dev/null
		if [ $? -eq 0 ]; then
			echo "Stopping nginx"
			systemctl stop nginx
		fi
		echo "Updating letsencrypt certificate for domain" $domain
		$path_to_certbot certonly --standalone -d $domain --standalone-supported-challenges tls-sni-01 --renew-by-default
		if [ $? -eq 0 ]; then
			echo "Certificate for" $domain "renewed"
		else
			echo "Something went wrong when renewing certificate for" $domamin
			exit $?
		fi
	else
		echo "Nothing to do for domain" $domain
	fi
done

if [ $? -gt 0 ]; then
    systemctl start nginx
fi;
